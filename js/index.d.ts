import { DIDDocument, ParsedDID, Resolver } from 'did-resolver';
export declare const PROVIDER_URI = "https://demo-sc-api-1.ssi.xpxsirius.io";
export declare const IPFS_ENDPOINT = "http://ipfs1-dev.xpxsirius.io:5001";
/**
 *
 * @param providerUri The ledger provider URL
 * @param contractAddress The contract address to register identity. This is not in use for
 * the current version
 * @param ipfsHost
 */
export declare function getResolver(providerUri?: string, ipfsHost?: string): {
    sirius: (did: string, parsed: ParsedDID, didResolver: Resolver) => Promise<DIDDocument | null>;
};
