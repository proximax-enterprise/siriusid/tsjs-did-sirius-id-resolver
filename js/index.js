"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getResolver = exports.IPFS_ENDPOINT = exports.PROVIDER_URI = void 0;
const tsjs_did_siriusid_1 = require("tsjs-did-siriusid");
exports.PROVIDER_URI = 'https://demo-sc-api-1.ssi.xpxsirius.io';
exports.IPFS_ENDPOINT = 'http://ipfs1-dev.xpxsirius.io:5001';
/**
 *
 * @param providerUri The ledger provider URL
 * @param contractAddress The contract address to register identity. This is not in use for
 * the current version
 * @param ipfsHost
 */
function getResolver(providerUri = exports.PROVIDER_URI, ipfsHost = exports.IPFS_ENDPOINT) {
    const registryProvider = new tsjs_did_siriusid_1.SiriusChainProvider(providerUri);
    const storageProvider = new tsjs_did_siriusid_1.IpfsStorageProvider(ipfsHost);
    async function resolve(did, parsed, didResolver) {
        const siriusId = await tsjs_did_siriusid_1.SiriusId.createInstance(registryProvider, storageProvider);
        try {
            const resolvedDidDocument = await siriusId.resolve(did);
            if (resolvedDidDocument) {
                const publicKey = resolvedDidDocument.publicKeys.map(p => {
                    return {
                        type: p.type,
                        id: p.id,
                        owner: p.owner,
                        publicKeyHex: p.publicKeyHex
                    };
                });
                const auth = resolvedDidDocument.authentications.map(a => {
                    return {
                        type: a.type,
                        publicKey: a.publicKey,
                    };
                });
                const service = resolvedDidDocument.services.map(s => {
                    return {
                        id: s.id,
                        serviceEndpoint: s.serviceEndpoint,
                        type: s.type,
                        description: s.description
                    };
                });
                const id = resolvedDidDocument.did.toString();
                const created = resolvedDidDocument.created.toISOString();
                const proof = {
                    type: resolvedDidDocument.proof.type,
                    created: resolvedDidDocument.proof.created.toISOString(),
                    creator: resolvedDidDocument.proof.creator,
                    signatureValue: resolvedDidDocument.proof.signature,
                    nonce: resolvedDidDocument.proof.nonce,
                };
                const didDocument = {
                    '@context': tsjs_did_siriusid_1.DEFAULT_CONTEXT_URL,
                    id: id,
                    publicKey: publicKey,
                    authentication: auth,
                    service: service,
                    created: created,
                    proof: proof,
                };
                return didDocument;
            }
            return null;
        }
        catch (error) {
            return null;
        }
    }
    return { 'sirius': resolve };
}
exports.getResolver = getResolver;
