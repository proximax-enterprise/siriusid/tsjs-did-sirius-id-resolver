import { getResolver } from '../src/index';
import { Resolver } from 'did-resolver';
import { mockDidDocument } from './data';
describe('SiriusId DID Resolver', () => {


  describe('getResolver', () => {
    it('should resolve SiriusId DID', async () => {
      const testDid = "did:sirius:2VhYrbauc2cCx9ZpCp5wrDtK7HKf7jrsvgoKBD4KgK";
      const siriusResolver = getResolver();
      const resolver = new Resolver(siriusResolver);
      const didDoc = await resolver.resolve(testDid);
      // console.log(JSON.stringify(didDoc));
      expect(didDoc).toEqual(mockDidDocument);
    });
  })
})