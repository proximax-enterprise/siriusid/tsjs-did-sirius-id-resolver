import { IDidDocument } from 'tsjs-did-siriusid';

export const mockDidDocument  = {
  "@context": "https://w3id.org/did/v1",
  "id": "did:sirius:2VhYrbauc2cCx9ZpCp5wrDtK7HKf7jrsvgoKBD4KgK",
  "publicKey": [
    {
      "type": "Ed25519VerificationKey2018",
      "id": "did:sirius:2VhYrbauc2cCx9ZpCp5wrDtK7HKf7jrsvgoKBD4KgK#keys-1",
      "owner": "did:sirius:2VhYrbauc2cCx9ZpCp5wrDtK7HKf7jrsvgoKBD4KgK",
      "publicKeyHex": "F474F89C654E17A5486D71F35A756DB444D39645568BE5AC932D511BA6537746"
    }
  ],
  "authentication": [
    {
      "type": "Ed25519VerificationKey2018",
      "publicKey": "did:sirius:2VhYrbauc2cCx9ZpCp5wrDtK7HKf7jrsvgoKBD4KgK#keys-1"
    }
  ],
  "service": [
    {
      "id": "did:sirius:2VhYrbauc2cCx9ZpCp5wrDtK7HKf7jrsvgoKBD4KgK;SiriusIdPublicProfile",
      "serviceEndpoint": "http://ipfs1-dev.xpxsirius.io:5001/ipfs/QmeMeZiei8PSZQ3Y9JGigwMVEbetY1U8SPv8qwp8CydxPv",
      "type": "SiriusIdPublicProfile",
      "description": "Verifiable Credential describing Sirius Id Public Profile"
    }
  ],
  "created": "2020-04-09T13:31:51.929Z",
  "proof": {
    "type": "Ed25519Signature2018",
    "created": "2020-04-09T13:31:51.939Z",
    "creator": "did:sirius:2VhYrbauc2cCx9ZpCp5wrDtK7HKf7jrsvgoKBD4KgK#keys-1",
    "signatureValue": "23AE2799F21D1906F2B375F9430BDB5C04DED81F2887FC15EDDA9DD8508A657DD86763EA1DC47DC57CF37C2EF5A718AEAEDD8D6AD111BA7F9D28743776789205",
    "nonce": {
      "words": [
        1460918072,
        2102700998
      ],
      "sigBytes": 8
    }
  }
}