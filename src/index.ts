import { DIDDocument, ParsedDID, Resolver, PublicKey, Authentication, ServiceEndpoint, LinkedDataProof } from 'did-resolver';
import { SiriusChainProvider, IpfsStorageProvider, SiriusId, DEFAULT_CONTEXT_URL } from 'tsjs-did-siriusid';

export const PROVIDER_URI = 'https://demo-sc-api-1.ssi.xpxsirius.io';
export const IPFS_ENDPOINT = 'http://ipfs1-dev.xpxsirius.io:5001'

/**
 * 
 * @param providerUri The ledger provider URL
 * @param contractAddress The contract address to register identity. This is not in use for 
 * the current version
 * @param ipfsHost 
 */
export function getResolver(providerUri: string = PROVIDER_URI, ipfsHost: string = IPFS_ENDPOINT) {

  const registryProvider = new SiriusChainProvider(providerUri);

  const storageProvider = new IpfsStorageProvider(ipfsHost);


  async function resolve(
    did: string,
    parsed: ParsedDID,
    didResolver: Resolver
  ): Promise<DIDDocument | null> {
    const siriusId = await SiriusId.createInstance(registryProvider, storageProvider);

    try {
      const resolvedDidDocument = await siriusId.resolve(did);
    
      if (resolvedDidDocument) {
        const publicKey: PublicKey[] = resolvedDidDocument.publicKeys.map(p => {
          return {
            type: p.type,
            id: p.id,
            owner: p.owner,
            publicKeyHex: p.publicKeyHex
          }
        });
  
        const auth: Authentication[] = resolvedDidDocument.authentications.map(a => {
          return {
            type: a.type,
            publicKey: a.publicKey,
          }
        })
  
        const service: ServiceEndpoint[] = resolvedDidDocument.services.map(s => {
          return {
           id: s.id,
           serviceEndpoint: s.serviceEndpoint,
           type: s.type,
           description: s.description
          }
        })
  
        const id: string = resolvedDidDocument.did.toString();
        const created: string = resolvedDidDocument.created.toISOString();
        const proof: LinkedDataProof =  {
          type: resolvedDidDocument.proof.type,
          created: resolvedDidDocument.proof.created.toISOString(),
          creator: resolvedDidDocument.proof.creator,
          signatureValue: resolvedDidDocument.proof.signature,
          nonce: resolvedDidDocument.proof.nonce,
        }
  
        const didDocument: DIDDocument = {
          '@context': DEFAULT_CONTEXT_URL,
          id: id,
          publicKey: publicKey,
          authentication: auth,
          service: service,
          created: created,
          proof: proof,
        };
  
        return didDocument;
      }
  
      return null;
    } catch (error) {
      return null;
    }
     
  }

  return { 'sirius': resolve }
}

