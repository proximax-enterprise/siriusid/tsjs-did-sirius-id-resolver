# SiriusId DID Resolver

DID Resolver for `sirius` DIDs based on the [DID-Resolver](https://github.com/decentralized-identity/did-resolver).

## Requirements

- NodeJS 10.x
- Typescript >= 3.5.3

## Installing the library

Install using [npm](https://www.npmjs.org/):

```shell
npm install tsjs-did-siriusid-resolver
```

or using [yarn](https://yarnpkg.com/):

```shell
yarn add tsjs-did-siriusid-resolver
```

## Getting started

Default Blockchain Ledger Provider

```ts
const ledgerProvider = 'https://demo-sc-api-1.ssi.xpxsirius.io'
```

Default Public Content Provider

```ts
const contentProvider = 'http://ipfs1-dev.xpxsirius.io:5001/
```

Provide the valid did document

```ts
const testDid = "did:sirius:2VhYrbauc2cCx9ZpCp5wrDtK7HKf7jrsvgoKBD4KgK";
```

Initiate siriusId resolver

```ts
const siriusResolver = getResolver(ledgerProvider, contentProvider);
````

Inject siriusId resolver to Universal resolver

```ts
const resolver = new Resolver(siriusResolver);
```

Resolve did document

```ts
const didDoc = await resolver.resolve(testDid);
console.log(didDoc);
```
